// What is Java and Why?
//A concurrent, class based object-oriented language specifically designed to have as few implementation dependencies as possible
//
//concurrent - can run multiple actions at a time
//class-based - explicit definition of what is needed in a program via blueprint s called classes
//
//Revolves heavily around the use of objects, whose properties are defined by the programmer
//
//Java allows for a more efficient way fo defining which classes are needed and which are not
//
//Java is more on OOP
//    Primary strength is its use of OOP paradigm
//    Allows for development of both desktop and web applications
//
//is a strongly typed language, which means that unlike dynamically-typed languages like JavaScript or PHP, it requires that the code to be compiled.
//
//needs to identify specific data type
//
//How java works
//
//Java source .java
//java compiler (javac)
//java byte code (.class)
//
//The jvm runs
//
//Compilers vs Interpreters
//Compilers need to compile the program before running
//Interpreter ?> javascript, upfront execution
//
//Primitive Data Types :
//Data type sized, used to matter a lot before because of hardware limitations.
//
//Nowadays, computes have better capacity, and thus we only have to think about size issues when we're optimizing
//


//Code Discussion.

// Main class
/* entry point of for our java program */
    // main class has 1 method inside, the main method => run code


public class Main {
    public static void main(String[] args) {
        // a statement that allows us to print the value of arguments passed into its terminal.
        System.out.println("Hello world!");
    }
}

// package - to group related classes.





















