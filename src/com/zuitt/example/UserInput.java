package com.zuitt.example;

import java.util.Scanner;
// instantiation
// class is used to get user input, and it is found in the java.util package.

public class UserInput {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in); // create a scanner object
        System.out.println("Enter username");

        String userName = myObj.nextLine(); // Read user input
      /*  System.out.println("Username is: " + userName);*/
        System.out.println(userName + userName);

        // java is strict regarding data types
    }
}
