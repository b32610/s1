package com.zuitt.example;

import java.util.Scanner;

/*
*
* Declare the following variables to store the user's information

*
*
* */

public class S1A1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("First Name");
        String firstName = userInput.nextLine();
        System.out.println("last Name");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade");
        double firstGrade = userInput.nextDouble();

        System.out.println("Second Subject Grade");
        double secondGrade = userInput.nextDouble();

        System.out.println("Third Subject Grade");
        double thirdGrade = userInput.nextDouble();

        double[] nums = {firstGrade, secondGrade, thirdGrade};
        float sum = 0;

        int i=0;
        while (i < nums.length) {
            sum += nums[i];
            i++;
        }

        float avefloat = (sum / nums.length);
        int average = Math.round(avefloat);


        System.out.println("Good day," + firstName + " " + lastName );
        System.out.println("Your grade average is: " + average);


    }
}
