
// a package in java is used to group related classes. Think of it as a folder in a file directory

// Package creation follows "reverse name notation" for the naming convention.
// DNS : Domain name system

package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {
        // Variable
        // variable = identifier
        // Syntax : dataType identifier
        int age;
        char middleInitial;

        // variable declaration vs initialization
        int x;
        int y = 0;

        // initialization after declaration
        x = 1;

        System.out.println("The value of y is" +y+ "  and the value of x is " + x);

        // Primitive data types
            // predefined within the Java programming language which is used for single valued variables with limited capabilities.

        // int - whole number values
        int wholeNumber = 1000000000;
        System.out.println(wholeNumber);

        // long
        long worldPopulation = 800000000000L;
        System.out.println(worldPopulation);

        // float decimal
        // f is added at the end of the number to recognized as float.
        // only occupies 7 decimal places
        float piFloat = 3.141592265359f;
        System.out.println(piFloat);

        // double - floating point values
        double doubleFloat = 3.1415922653589;
        System.out.println(doubleFloat);

        // char - single character
        // uses single quotes
        char letter = 'a';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // constants
        // Java uses the "final" keyword to the variable's value that cannot be changed.
        final int PRINCIPAL = 3000; // all caps to define as a constant
        System.out.println(PRINCIPAL);

        // Non-primitive data type
            // also known as reference data types refer to instances or objects.
            // do not directly store the value of a variable, but rather remembers the reference to that variable.

        // String
            // Store a sequence or array of characters.
            // String are actually object that can use methods.

        String userName = "JSmith";
        System.out.println(userName);

        // Sample String Method.
        int stringLength = userName.length();
        System.out.println(stringLength);





    }
}


