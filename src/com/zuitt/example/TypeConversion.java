package com.zuitt.example;

import java.util.Scanner;
// primitive stores multiple values


public class TypeConversion {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you?");

        // first method
        // double age = Double.parseDouble(userInput.nextLine());

        // Second Method
        double age = userInput.nextDouble();
        System.out.println("This is a confirmation that you are " + age + " years old");

        System.out.println("Enter a first number");
        int num1 = userInput.nextInt();

        System.out.println("Enter a Second number");
        int num2 = userInput.nextInt();

        int sum = num1 + num2;
        System.out.println("the sum of both numbers are: " + sum);

    }
}
